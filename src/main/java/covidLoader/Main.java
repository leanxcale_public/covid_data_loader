package covidLoader;

import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.session.Credentials;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import covidLoader.transformer.TerritoryTransformer;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import covidLoader.transformer.NationalTransformer;
import covidLoader.transformer.Transformer;

public class Main {

  private static Logger log = LoggerFactory.getLogger(Main.class);

  public static void main(String args[]){

    String url = args[0];
    String fileNational = args[1];
    String fileTerritory = args[2];

    String database = "covid";
    String tableNameNat = "data_nat";
    String tableNameTerritory = "data_ccaa";

    Settings settings = new Settings();
    settings.credentials(new Credentials("APP", "APP".toCharArray(), database));

    Transformer transformerNational = new NationalTransformer();
    Loader loaderNational = new Loader(fileNational, tableNameNat, transformerNational);

    Transformer transformerTerritory = new TerritoryTransformer();
    Loader loaderTerritory = new Loader(fileTerritory, tableNameTerritory, transformerTerritory);

    try(Session session = SessionFactory.newSession(url, settings)){

      loaderNational.load(session);
      loaderTerritory.load(session);
    }
    catch (LeanxcaleException | IOException e) {

      log.error("Error loading data: {}",e.getMessage(), e);
    }
  }
}
