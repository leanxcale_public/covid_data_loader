package covidLoader;

import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.tuple.Tuple;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import covidLoader.transformer.Transformer;

public class Loader {

  private static final Logger log = LoggerFactory.getLogger(Loader.class);

  private int commitSize;
  private boolean skipHeader;

  private String file;
  private String tableName;

  private Transformer transformer;

  public Loader(String file, String tableName, Transformer transformer) {

    this.file = file;
    this.transformer = transformer;
    this.tableName = tableName;

    this.commitSize = 10000;
    this.skipHeader = true;
  }

  public void setCommitSize(int commitSize) {
    this.commitSize = commitSize;
  }

  public void setSkipHeader(boolean skipHeader) {
    this.skipHeader = skipHeader;
  }

  public void load(Session session)
      throws IOException{

    transformer.createTable(session,tableName);

    log.info("Starting to load file {}", file);

    try(BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(file)))){

      Table table = session.database().getTable(tableName);
      Tuple tuple = table.createTuple();

      int total = 0;
      int commitCount = 0;

      if(skipHeader){
        reader.readLine();
      }

      String line;
      while((line = reader.readLine())!=null){

        transformer.fillTuple(line, tuple);
        table.insert(tuple);

        total++;
        commitCount++;

        if(commitCount>=commitSize){

          log.info("Committing at line {}", total);

          session.commit();
          commitCount = 0;
        }
      }

      if(commitCount > 0){

        log.info("Committing last {} lines", commitCount);
        session.commit();
      }
    }

    log.info("Loaded file {}", file);
  }
}
