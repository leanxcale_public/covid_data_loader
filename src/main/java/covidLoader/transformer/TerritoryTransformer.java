package covidLoader.transformer;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

import com.leanxcale.kivi.database.Field;
import com.leanxcale.kivi.database.Type;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.tuple.Tuple;
import java.sql.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TerritoryTransformer implements Transformer {

  private static final Logger log = LoggerFactory.getLogger(Transformer.class);

  private static final String TERRITORY_FIELD = "TERRITORY";
  private static final String DATE_FIELD = "DATE";
  private static final String CONFIRMED_FIELD = "CASES";
  private static final String DEATHS_FIELD = "DEATHS";
  private static final String ICU_FIELD = "ICU";
  private static final String HOSPITAL_FIELD = "HOSPITAL";

  @Override
  public void createTable(Session session, String tableName) {

    if(session.database().tableExists(tableName)){

      log.info("Dropping table {}", tableName);

      session.database().dropTable(tableName);
    }

    log.info("Creating table {}", tableName);

    session.database()
        .createTable(
            tableName,
            asList(
                new Field(TERRITORY_FIELD, Type.STRING),
                new Field(DATE_FIELD, Type.DATE)
            ),
            asList(
                new Field(CONFIRMED_FIELD, Type.INT),
                new Field(DEATHS_FIELD, Type.INT),
                new Field(ICU_FIELD, Type.INT),
                new Field(HOSPITAL_FIELD, Type.INT)
            )
        );

    log.info("Table {} created", tableName);
  }

  @Override
  public void fillTuple(String line, Tuple tuple) {

    String fields[] = line.split("," ,-1);

    tuple.putString(TERRITORY_FIELD, fields[2]);
    tuple.putDate(DATE_FIELD, Date.valueOf(fields[0]));

    String cases = fields[4];

    String deaths = fields[8];
    String icu = fields[7];
    String hospital = fields[6];

    tuple.putInteger(CONFIRMED_FIELD, convertField(cases));
    tuple.putInteger(DEATHS_FIELD, convertField(deaths));
    tuple.putInteger(ICU_FIELD, convertField(icu));
    tuple.putInteger(HOSPITAL_FIELD, convertField(hospital));
  }
}
