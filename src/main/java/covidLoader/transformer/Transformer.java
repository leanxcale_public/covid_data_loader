package covidLoader.transformer;

import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.tuple.Tuple;

public interface Transformer {

  /**
   * Create a table with the format needed for this dataset
   *
   * @param session
   * @param tableName
   */
  void createTable(Session session, String tableName);

  /**
   * Fills a tuple with the date contained in the line
   *
   * @param line
   * @param tuple
   */
  void fillTuple(String line, Tuple tuple);

  /**
   * Transforms a String to an int, convrting an emptu String into am integer
   * @param field
   * @return
   */
  default int convertField(String field){

    if(field == null || field.isEmpty()){
      return 0;
    }

    return Integer.valueOf(field);
  }
}
